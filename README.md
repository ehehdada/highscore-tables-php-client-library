# highscore-tables-php-client-library
This is a library that can be imported in PHP projects to connect to our Highscore Tables API

# Create new highscores table

Please visit https://highscores.ehehdada.com/new_account to create your new highscores table.

# Install with Composer

```bash
curl -sS https://getcomposer.org/installer | php
```

Next, run the Composer command to install the latest stable version of this library:

```bash
composer require lucentinian/highscore-tables-php-client-library
```

After installing, you need to require Composer's autoloader:

```php
require 'vendor/autoload.php';
```

# Documentation

More information can be found in the online documentation at
https://highscores.ehehdada.com/
