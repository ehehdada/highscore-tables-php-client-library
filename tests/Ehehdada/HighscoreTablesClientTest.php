<?php

namespace Ehehdada\Tests;

require 'src/Ehehdada/HighscoreTablesClient.php';

use Ehehdada;

class HighscoreTablesClientTest extends \PHPUnit_Framework_TestCase
{
	
	private $config;
	
	public function __construct()
	{
		$this->config = array(
			"table_key" => "test",
			"table_name" => "test",
			"shared_secret" => "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08"
		);

	}
	
	public function testGetTable() 
	{
		$hstDemo = new \Ehehdada\HighscoreTablesClient($this->config);
		$result = $hstDemo->getTable($status);
		echo "get table result\n".print_r($result,true)."\n";
		$this->assertEquals(200, $status);
	}
	
	public function testDeleteTable() 
	{
		$hstDemo = new \Ehehdada\HighscoreTablesClient($this->config);
		$result = $hstDemo->deleteTable($status);
		echo "delete table result\n".print_r($result,true)."\n";
		$this->assertEquals(200, $status);
	}
	
	public function testCheckElegibility() 
	{
		$hstDemo = new \Ehehdada\HighscoreTablesClient($this->config);
		$result = $hstDemo->checkElegibility($status,1234,"Myself");
		echo "check elegibility result\n".print_r($result,true)."\n";
		$this->assertEquals(200, $status);
	}
	
	public function testAddRecord() 
	{
		$hstDemo = new \Ehehdada\HighscoreTablesClient($this->config);
		$result = $hstDemo->addRecord($status,1234,"Myself",time() * 1000,"en_EN");
		echo "add record result\n".print_r($result,true)."\n";
		$this->assertEquals(200, $status);
	}
	
	public function testIntegration() 
	{
		echo "test integration\n";
		$this->testDeleteTable();
		$this->testAddRecord();
		$this->testAddRecord();
		$hstDemo = new \Ehehdada\HighscoreTablesClient($this->config);
		$result = $hstDemo->getTable($status);
		echo "integration: get table result\n".print_r($result,true)."\n";
		$this->assertEquals(200, $status);
		$this->assertEquals(2, count($result));
	}
}
