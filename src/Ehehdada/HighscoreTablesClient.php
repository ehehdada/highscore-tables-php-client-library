<?php 

namespace Ehehdada;

/**
 * 
 */
class HighscoreTablesClient 
{
	const BASE_URL = "https://highscores.ehehdada.com";
	private $table_name;
	private $table_key;
	private $urlencoded_table_key;
	private $shared_secret;
	
	/**
	 * @param array	$config
	 */
	public function __construct(array $config) 
	{
		$this->table_name = urlencode($config['table_name']);
		$this->table_key = $config['table_key'];
		$this->urlencoded_table_key = urlencode($this->table_key);
		$this->shared_secret = $config['shared_secret'];
	}
	
	/**
	 * 
	 */
	private function calculateFingerPrint($request_uri, $body, $timestamp) 
	{
		$string_to_hash = 
			$request_uri
			."|"
			.$body
			."|"
			.$timestamp
			."|"
			.$this->table_key
			."|"
			.$this->shared_secret
			;
		return hash("sha256",$string_to_hash."|".strlen($string_to_hash));
	}
	
	/**
	 * 
	 */
	public function getTable(&$status) 
	{
		$request_uri = "/api/".$this->table_name."?table_key=".$this->urlencoded_table_key;
		$s = curl_init();
		curl_setopt($s,CURLOPT_URL,self::BASE_URL.$request_uri);
		curl_setopt($s,CURLOPT_HTTPHEADER,array(
			"X-Requested-With: XMLHttpRequest"
		));
		curl_setopt($s,CURLOPT_RETURNTRANSFER,true);
		$result = curl_exec($s);
		$status = curl_getinfo($s,CURLINFO_HTTP_CODE);
		curl_close($s);
		if ($status == 200) 
		{
			return json_decode($result);
		}
		return false;
	}
	
	/**
	 * 
	 */
	public function checkElegibility(&$status,$score,$by_whom = false) 
	{
		$request_uri = "/api/".$this->table_name."/check/?score=$score";
		if ($by_whom) 
		{
			$request_uri .= "&by_whom=".urlencode($by_whom);
		}
		$timestamp = time() * 1000;
		$fingerprint = $this->calculateFingerPrint($request_uri,"",$timestamp);
		$s = curl_init();
		curl_setopt($s,CURLOPT_URL,self::BASE_URL.$request_uri);
		curl_setopt($s,CURLOPT_HTTPHEADER,array(
			"fingerprint: $fingerprint",
			"timestamp: $timestamp",
			"tablekey: ".$this->table_key,
			"X-Requested-With: XMLHttpRequest"
		));
		curl_setopt($s,CURLOPT_RETURNTRANSFER,true);
		$result = curl_exec($s);
		$status = curl_getinfo($s,CURLINFO_HTTP_CODE);
		curl_close($s);
		if ($status == 200) 
		{
			return json_decode($result);
		}
		return false;
	}
	
	/**
	 * 
	 */
	public function getServerTimestamp(&$status)
	{
		$request_uri = "/api/timestamp";
		$s = curl_init();
		curl_setopt($s,CURLOPT_URL,self::BASE_URL.$request_uri);
		curl_setopt($s,CURLOPT_RETURNTRANSFER,true);
		$result = curl_exec($s);
		$status = curl_getinfo($s,CURLINFO_HTTP_CODE);
		curl_close($s);
		if ($status == 200) 
		{
			return json_decode($result);
		}
		return false;
	}
	
	/**
	 * 
	 */
	public function addRecord(&$status,$score,$by_whom,$when_happened,$locale)
	{
		$body = json_encode(array(
			"score" => $score,
			"by_whom" => $by_whom,
			"when_happened" => $when_happened,
			"locale" => $locale 
		));
		$request_uri = "/api/".$this->table_name;
		$timestamp = time() * 1000;
		$fingerprint = $this->calculateFingerPrint($request_uri,$body,$timestamp);
		$s = curl_init();
		curl_setopt($s,CURLOPT_URL,self::BASE_URL.$request_uri);
		curl_setopt($s,CURLOPT_HTTPHEADER,array(
			"fingerprint: $fingerprint",
			"timestamp: $timestamp",
			"tablekey: ".$this->table_key,
			"X-Requested-With: XMLHttpRequest"
		));
		curl_setopt($s,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($s,CURLOPT_POST,true);
		curl_setopt($s,CURLOPT_POSTFIELDS,$body);
		$result = curl_exec($s);
		$status = curl_getinfo($s,CURLINFO_HTTP_CODE);
		curl_close($s);
		if ($status == 200) 
		{
			return json_decode($result);
		}
		return false;
	}
	
	/**
	 * 
	 */
	public function deleteTable(&$status) 
	{
		$request_uri = "/api/".$this->table_name."/deleted";
		$timestamp = time() * 1000;
		$fingerprint = $this->calculateFingerPrint($request_uri,"",$timestamp);
		$s = curl_init();
		curl_setopt($s,CURLOPT_URL,self::BASE_URL.$request_uri);
		curl_setopt($s,CURLOPT_HTTPHEADER,array(
			"fingerprint: $fingerprint",
			"timestamp: $timestamp",
			"tablekey: ".$this->table_key,
			"X-Requested-With: XMLHttpRequest"
		));
		curl_setopt($s,CURLOPT_RETURNTRANSFER,true);
		$result = curl_exec($s);
		$status = curl_getinfo($s,CURLINFO_HTTP_CODE);
		curl_close($s);
		return $status == 200;
	}
}
